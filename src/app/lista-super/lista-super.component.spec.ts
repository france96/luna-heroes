import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaSuperComponent } from './lista-super.component';

describe('ListaSuperComponent', () => {
  let component: ListaSuperComponent;
  let fixture: ComponentFixture<ListaSuperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaSuperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaSuperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
