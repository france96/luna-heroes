import { Component, OnInit } from '@angular/core';

import { Hero } from './hero.model';

@Component({
  selector: 'app-lista-super',
  templateUrl: './lista-super.component.html',
  styleUrls: ['./lista-super.component.scss']
})
export class ListaSuperComponent implements OnInit {
  heroes: Hero[] =[
    new Hero('Superman',30,'M','Silenzioso','nessuno','Ragnatela'),
    new Hero('Batman',45,'M','Agile','nessuno','Capata'),
    new Hero('Ciccia',31,'F','Lenta','nessuno','salto')
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
