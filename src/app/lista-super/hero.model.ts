
export class Hero{
    public name:string;
    public eta: number;
    public sesso:string;
    public segniParticolari:string;
    public note:string;
    public azioni:string;

    constructor(name:string,eta:number, sesso:string, segniParticolari:string, note:string,azioni:string){
        this.name=name;
        this.eta=eta;
        this.sesso=sesso;
        this.segniParticolari=segniParticolari;
        this.note=note;
        this.azioni=azioni;
    }
}