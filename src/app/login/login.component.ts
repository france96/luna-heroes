import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { UsernameValidators } from '../common/validators/username.validators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  //gestico il form per il login, Validators può essere anche un array (dove inserire multipli controlli)
  form= new FormGroup({
    username: new FormControl('',[
     Validators.required,
     Validators.minLength(5),
     UsernameValidators.cannotContainSpace 
    ]),
    password: new FormControl('',Validators.required)
  });

  get username(){
    return this.form.get('username');
  }

  message='';
  nome='';
  public name= "Alex";

  constructor() { }

  navigateToFirst() {
  }

  ngOnInit(): void {
  }
  
 

 
}
